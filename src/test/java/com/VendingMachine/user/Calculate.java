package com.VendingMachine.user;

public class Calculate implements Calculator{


	public int calculateTotal(CoinBundle enteredCoins) {
		
		return enteredCoins.getTotal();
	}


	public CoinBundle calculateChange(int amountToReturn) {
		CoinBundle change = new CoinBundle(new int[4]);
		int remainingAmt = amountToReturn;
		
		change.numOf1CentCoins  = remainingAmt/1;
		remainingAmt = remainingAmt%1;
		
		change.numOf5CentCoins  = remainingAmt/5;
		remainingAmt = remainingAmt%5;
		
		change.numOf10CentCoins  = remainingAmt/10;
		remainingAmt = remainingAmt%10;
		
		change.numOf25CentCoins  = remainingAmt/25;
		remainingAmt = remainingAmt%25;
		
		return change;
	}

}
