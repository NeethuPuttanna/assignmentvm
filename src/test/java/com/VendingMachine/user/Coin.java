package com.VendingMachine.user;

public enum Coin {
		ONE_CENT(1), FIVE_CENT(5), TEN_CENT(10), TWENTYFIVE_CENT(25);
	
	private int value;
	
	private Coin(int value){
		this.value = value;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public static int[] parseCoins(String coins) {
		String[] numberOfCoinsInText = coins.split(",");
		int[] result = new int[numberOfCoinsInText.length];
		
		for(int index=0;index<numberOfCoinsInText.length;index++) {
			result[index]=Integer.parseInt(numberOfCoinsInText[index]);
		}
		return result;
	}

}
