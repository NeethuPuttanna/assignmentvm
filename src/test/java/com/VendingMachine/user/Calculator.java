package com.VendingMachine.user;

public interface Calculator {
	int calculateTotal(CoinBundle enteredCoins);
	CoinBundle calculateChange(int amountToReturn);
}
