package com.VendingMachine.user;

public interface VendingMachine {
	
	void displayAllProducts();
	void selectProduct(int product);
	void displayEnteredCoinsMessage();
	void enterCoins(int... coins);
	void displayChange();
}
