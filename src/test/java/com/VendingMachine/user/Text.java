package com.VendingMachine.user;

import io.cucumber.messages.Messages.Meta.Product;

public class Text implements VendingMachine{
	
	private int productSelected;
	private CoinBundle change;

	public void displayAllProducts() {
		System.out.println("*************VENDING MACHINE**************");
		System.out.println("The Products available");
		
		for(products product: products.values()) {
			System.out.println("   "+product.getID()+" "+product.name()+"-Price:"+product.getPrice());
		}
	}

	
	public void selectProduct(int product) {
		this.productSelected = product;
	}

	public void displayEnteredCoinsMessage() {
		System.out.println("Please enter coins: 1 CENT, 5 CENT, 10 CENT or 25 CENT");
		System.out.println("            ");
		System.out.println("Enter coins as instructed below: If you would enter two 5 Cent coins then enter as: 0,2,0,0");
		System.out.println("Please enter coins:");
	}

	
	public void enterCoins(int[] coins) {
		Calculator calc = new Calculate();
		products product = products.valueOf(this.productSelected);
		int total =calc.calculateTotal(new CoinBundle(coins));
		int changeAmt = total-product.getPrice();
		this.change=calc.calculateChange(changeAmt);
	}

	
	public void displayChange() {
		System.out.println("---------------------------------------------------");
		System.out.println("Collect your item and your change is :"+change.getTotal()+"cents splitted as follows");
		System.out.println("1 cent coins = "+ change.numOf1CentCoins);
		System.out.println("5 cent coins = "+ change.numOf5CentCoins);
		System.out.println("10 cent coins = "+ change.numOf10CentCoins);
		System.out.println("25 cent coins = "+ change.numOf25CentCoins);
	}

}
