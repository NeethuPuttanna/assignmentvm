package com.VendingMachine.user;
	
	public enum products{
		COKE(1, 25), PEPSI(2, 25), SODA(3, 45);
		
		private int id;
		private int price;
		
		products(int id, int price){
			this.id = id;
			this.price=price;
		}
		
		public int getID() {
			return this.id;
		}
		
		public int getPrice() {
			return this.price;
		}
		
		public static products valueOf(int productSelected) {
			for(products product: products.values()) {
				if(productSelected == product.getID()) {
					return product;
				}
			}
			return null;
		}
	}
	
