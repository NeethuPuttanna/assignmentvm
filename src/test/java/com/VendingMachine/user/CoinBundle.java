package com.VendingMachine.user;

public class CoinBundle {
	
	public int numOf1CentCoins;
	public int numOf5CentCoins;
	public int numOf10CentCoins;
	public int numOf25CentCoins;
	
	public CoinBundle(int... enteredCoins) {
		this.numOf1CentCoins=enteredCoins[0];
		this.numOf5CentCoins=enteredCoins[1];
		this.numOf10CentCoins=enteredCoins[2];
		this.numOf25CentCoins=enteredCoins[3];
	}
	
	public int getTotal() {
		int total = 0;
		total = total+this.numOf1CentCoins*1;
		total = total+this.numOf5CentCoins*5;
		total = total+this.numOf10CentCoins*10;
		total = total+this.numOf25CentCoins*25;
		return total;
	}


}
