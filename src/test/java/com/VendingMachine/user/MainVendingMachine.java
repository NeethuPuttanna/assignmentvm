package com.VendingMachine.user;

import java.util.Scanner;

public class MainVendingMachine {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		VendingMachine vendingMachine = new Text();
		
		vendingMachine.displayAllProducts();
		
		System.out.println("Enter the selected product: ");
		String productSelected = keyboard.nextLine();
		int prodSelected = Integer.parseInt(productSelected);
		
		System.out.println("Product selected by user = "+ prodSelected);
		vendingMachine.selectProduct(prodSelected);
		
		vendingMachine.displayEnteredCoinsMessage();
		
		String userEnteredCoins = keyboard.nextLine();
		
		int[] coinsEntered = Coin.parseCoins(userEnteredCoins);
		vendingMachine.enterCoins(coinsEntered);
		vendingMachine.displayChange();	
	}

}
